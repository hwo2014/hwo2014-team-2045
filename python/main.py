import json
import socket
import sys
import math
import copy
import time
import collections


class History():
    def __init__(self, track):
        self.track = track
        self.history = []
        self.prevSpeed = 0

    def add(self, carPosition):
        self.history.append(carPosition)
        self.getCurrentPiece()['speed'] = self.getSpeed()

    def getCurrentPiece(self):
        return self.history[-1]['data'][0]

    def getCurPiecePos(self):
        return self.getCurrentPiece()['piecePosition']

    def getPrevPiecePos(self):
        if len(self.history) < 2:
            return self.getCurrentPiece()['piecePosition']
        return self.history[-2]['data'][0]['piecePosition']

    def getCurInPieceDist(self):
        return self.getCurPiecePos()['inPieceDistance']

    def getPrevInPieceDist(self):
        if len(self.history) < 2:
            return 0
        return self.history[-2]['data'][0]['piecePosition']['inPieceDistance']

    def getCurPieceIdx(self):
        return self.history[-1]['data'][0]['piecePosition']['pieceIndex']

    def getPrevSpeed(self):
        if len(self.history) < 2:
            return 0
        return self.history[-2]['data'][0]['speed']

    def getSpeed(self):
        if len(self.history) < 2:
            return 0
        track = self.track
        prevCarPos = self.history[-2]['data'][0]['piecePosition']
        prevPieceDist = prevCarPos['inPieceDistance']
        prevPiece = track.getPiece(prevCarPos['pieceIndex'])
        prevCarLaneIdx = prevCarPos['lane']['startLaneIndex']
        prevPart = track.getPart(prevCarPos['pieceIndex'])

        curCarPos = self.history[-1]['data'][0]['piecePosition']
        curPieceDist = curCarPos['inPieceDistance']

        if 'length' in prevPiece:
            prevLength = prevPiece['length']
        else:
            for lane in prevPiece['lanes']:
                if lane['index'] == prevCarLaneIdx:
                    sign = 1
                    if (prevPart['turn'] < 0 and lane['distanceFromCenter'] < 0) or (prevPart['turn'] > 0 and lane['distanceFromCenter'] > 0):
                        sign = -1

                    prevLength = ((math.pi*(prevPiece['radius'] + (math.fabs(lane['distanceFromCenter']) * sign)))/180)*math.fabs(prevPiece['angle'])

        if prevPieceDist < curPieceDist:
            speed = curPieceDist - prevPieceDist
            return speed
        elif prevPieceDist > curPieceDist:
            speed = prevLength - prevPieceDist + curPieceDist
            return speed
        return 0

    def findOptPath(self):
        return 0

    def getGlobalPosition(self):
        # history ?
        currentPieceDist = self.getCurInPieceDist()
        pieceIdx = self.getCurPieceIdx()
        part = self.track.getPart(pieceIdx)
        globPos = currentPieceDist
        for partPieceIdx in part['pieces']:
            if pieceIdx == partPieceIdx:
                break
            globPos += self.track.getPieceLength(partPieceIdx, self)
        return globPos

    def distanceToNextTurn(self, nextTurnPart):
        pieceIndex = self.getCurPieceIdx()
        currentPart = self.track.getPart(pieceIndex)
        currentPartIdx = self.track.derictionParts.index(currentPart)
        length = 0
        if 'length' not in currentPart:
            laneIdx = self.getCurPiecePos()['lane']['endLaneIndex']
            for lane in currentPart['lanes']:
                if lane['index'] == laneIdx:
                    length += lane['length'] - self.getGlobalPosition()
                    break
        else:
            length = currentPart['length'] - self.getGlobalPosition()
        someIndex = self.track.derictionParts.index(self.track.getNextPart(pieceIndex))
        if someIndex - currentPartIdx > 1:
            while someIndex < len(self.track.derictionParts) or someIndex != self.track.derictionParts.index(nextTurnPart):
                if 'length' not in self.track.getPart(someIndex) and len(self.track.getPart(someIndex).keys()) > 0:
                    print self.track.getPart(someIndex)
                    laneIdx = self.getCurPiecePos()['lane']['endLaneIndex']
                    for lane in self.track.getPart(someIndex)['lanes']:
                        if lane['index'] == laneIdx:
                            length += lane['length']
                            break
                else:
                    length += self.track.getPart(someIndex)['length']
                someIndex += 1
        # for last piece
        if self.track.derictionParts.index(currentPart) == len(self.track.derictionParts) - 1 and 'length' in self.track.derictionParts[0]:
            length += self.track.derictionParts[0]['length']
        return length


class Track():
    def __init__(self, data):
        self.data = data
        self.defaultDict = copy.copy(data)
        self.trackPieces = self.defaultDict['race']['track']['pieces']
        self.track = self.defaultDict['race']['track']
        self.derictionParts = []
        self.mu = 0.045

        self.reBuildTrack()

        print "+++++++++++"
        print self.trackPieces
        print "+++++++++++"

    def getPiece(self, index):
        return self.trackPieces[index]

    def getPieceLength(self, index, history):
        length = 0
        if 'angle' in self.getPiece(index):
            laneIdx = history.getCurPiecePos()['lane']['endLaneIndex']
            for lane in self.getPiece(index)['lanes']:
                if lane['index'] == laneIdx:
                    length = lane['length']
                    break
        else:
            length = self.getPiece(index)['length']
        return length

    def getPart(self, index):
        for part in self.derictionParts:
            if index in part['pieces']:
                return part
        return {}

    def getNextPart(self, index):
        for idx, part in enumerate(self.derictionParts):
            nextIdx = idx + 1
            if idx == len(self.derictionParts) - 1:
                nextIdx = 0
            if index in part['pieces']:
                return self.derictionParts[nextIdx]
        return {}

    def getPartLength(self, index):
        return self.getPart(index)['length']

    # def initTrack(self):
    #     lanes = copy.copy(self.track['lanes'])
    #     for lane in lanes:
    #         laneParts = []
    #         for index, piecece in enumerate(self.trackPieces):
    #             if 'angle' in piece:

    def reBuildTrack(self):
        # direction part
        # length, angle, radius
        derictionParts = []
        for index, piece in enumerate(self.trackPieces):
            if 'length' in piece:
                if len(derictionParts) > 0 and 'length' in derictionParts[-1]:
                    derictionParts[-1]['length'] = derictionParts[-1]['length'] + piece['length']
                    derictionParts[-1]['pieces'].append(index)
                else:
                    derictionParts.append({'length': piece['length'], 'pieces': [index]})
            if 'angle' in piece and 'radius' in piece:
                piece['lanes'] = []
                if len(derictionParts) > 0 and 'angle' in derictionParts[-1]\
                        and math.copysign(1, derictionParts[-1]['angle']) == math.copysign(1, piece['angle'])\
                        and derictionParts[-1]['radius'] == piece['radius']:
                        #and derictionParts[-1]['angle'] == piece['angle']\
                    derictionParts[-1]['angle'] = derictionParts[-1]['angle'] + piece['angle']
                    derictionParts[-1]['pieces'].append(index)
                else:
                    derictionParts.append({
                        'angle': piece['angle'],
                        'radius': piece['radius'],
                        'pieces': [index],
                        'turn': math.copysign(1, piece['angle'])
                    })

                for lane in self.track['lanes']:
                    distanceFromCenter = lane['distanceFromCenter']
                    if derictionParts[-1]['turn'] > 0:
                        distanceFromCenter = distanceFromCenter * -1
                    length = math.fabs(((math.pi*(piece['radius'] + distanceFromCenter))/180)*piece['angle'])
                    laneNew = copy.copy(lane)
                    laneNew['length'] = length
                    piece['lanes'].append(laneNew)
                #derictionParts[-1]['lanes'] = copy.copy(self.track['lanes'])
                #print self.track['lanes']

        for part in derictionParts:
            for pieceIdx in part['pieces']:
                if 'lanes' in self.trackPieces[pieceIdx]:
                    if 'lanes' not in part:
                        copiedLanes = copy.deepcopy(self.trackPieces[pieceIdx]['lanes'])
                        part['lanes'] = copiedLanes
                    elif 'lanes' in part:
                        for idx, lane in enumerate(self.trackPieces[pieceIdx]['lanes']):
                            part['lanes'][idx]['length'] = part['lanes'][idx]['length'] + lane['length']

        # save all directional parts
        print derictionParts
        self.derictionParts = derictionParts


class Player1Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.brake = False
        self.passTurn = []

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": "keimola", "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch(self, switch):
        self.msg("switchLane", switch)

    def activateTurbo(self):
        self.msg("turbo", "Go")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join_race()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        print data
        self.track = Track(data)
        self.history = History(self.track)
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        # start timer
        self.throttle(1)
        self.track.time = time.time()
        self.ping()

    def getDistance(self, acceleration, speedRequired, speedActual):
        return ((speedActual * speedActual) - (speedRequired * speedRequired)) / 2*acceleration

    def checkForTurn(self):
        track = self.track.trackPieces
        pieceIndex = self.history.getCurPieceIdx()
        if pieceIndex == len(track) - 1:
            nextPieceIndex = 0
        else:
            nextPieceIndex = pieceIndex + 1
        currentPieceStartLaneIdx = self.history.getCurPiecePos()['lane']['startLaneIndex']
        nextPiece = track[nextPieceIndex]

        if 'switch' in nextPiece:
            # look for next turn

            partWithTurnIdx = self.track.derictionParts.index(self.track.getNextPart(pieceIndex))
            nextTurn = None
            while partWithTurnIdx < len(self.track.derictionParts):
                if 'turn' not in self.track.derictionParts[partWithTurnIdx]:
                    partWithTurnIdx += 1
                else:
                    nextTurn = self.track.derictionParts[partWithTurnIdx]
                    break

            if nextTurn is not None:
                testLane = sorted(nextTurn['lanes'], key=lambda k: k['length'])
                #print testLane[0]['index'], currentPieceStartLaneIdx, testLane, partWithTurnIdx
                if testLane[0]['index'] != currentPieceStartLaneIdx:
                    if nextTurn['turn'] > 0:
                        #print "Right"
                        self.switch('Right')
                    else:
                        #print "Left"
                        self.switch('Left')

    def getBreakingLength(self, speedRequired):

        #V^2/(2*a)

        speed = self.history.getSpeed()
        realSpeed = (speed * 10)
        realPrevSpeed = (self.history.getPrevSpeed() * 10)

        speedDiff = math.fabs(realSpeed - speedRequired)
        speedDiff2 = realSpeed + speedRequired

        acceleration = math.fabs(realSpeed - realPrevSpeed)
        L = 0
        if acceleration != 0:
            L = ((speedDiff * speedDiff) / (2*acceleration))
        return L

    def on_car_positions(self, data):

        self.history.add(data)
        speed = self.history.getSpeed()
        realSpeed = speed * 10
        realPrevSpeed = self.history.getPrevSpeed() * 10
        mu = self.track.mu * 10

        track = self.track.trackPieces
        pieceIndex = self.history.getCurPieceIdx()
        currentCarPosition = self.history.getCurrentPiece()

        #print "Speed", mu*9.8

        #print currentPiece
        self.checkForTurn()
        
        #return 0


        if pieceIndex == len(track) - 1:
            nextPieceIndex = 0
        else:
            nextPieceIndex = pieceIndex + 1

        #self.throttle(1)

        #lengthToTurn = 

        self.checkForTurn()

        #print self.getBreakingLength(), self.history.getGlobalPosition(), realSpeed

        # if 'angle' in track[nextPieceIndex] and 'length' in track[pieceIndex] and track[pieceIndex]['length']-10 <= self.history.getCurInPieceDist():
        #     print "BREAK"
        #     self.throttle(1)

        #currentPiece['piecePosition']['lap']
        curPart = self.track.getPart(pieceIndex)

        #print self.track.derictionParts.index(curPart), len(self.track.derictionParts) - 1
        # print self.track.derictionParts.index(curPart)

        # if self.track.derictionParts.index(curPart) - 1 == len(self.track.derictionParts) - 2 and hasattr(self, 'turbo'):
        #     if hasattr(self, 'timer'):
        #         self.timer += 1
        #     self.activateTurbo()

        # #turboDurationTicks
        # if hasattr(self, 'timer') and self.timer < 31:
        #     self.throttle(1)
        #     print "TIMER: ", self.timer, self.turbo
        #     return 0

        if 'radius' in self.track.getPart(pieceIndex):
            # print "Go"
            self.brake = False

            nextPart = self.track.getPart(nextPieceIndex)

            currentPieceStartLaneIdx = self.history.getCurPiecePos()['lane']['startLaneIndex']
            sign = 1
            if (curPart['turn'] < 0 and currentPieceStartLaneIdx == 0) or (curPart['turn'] > 0 and currentPieceStartLaneIdx > 0):
                sign = -1

            radius = curPart['radius']+(10*sign)

            speedRequired = math.sqrt(radius*9.8*self.track.mu) * 10            

            curAngle = (self.history.getGlobalPosition() / ((math.pi*radius)/180))

            halfCurAngle = math.fabs(curPart['angle']) * 0.65

            # if curAngle > 90 and curAngle < 10 and curPart['angle'] > 180:
            #     self.throttle(1)
            # elif curAngle > 180 and curAngle < 190:
            #     self.throttle(1)
            # else:
            #     self.throttle(speedRequired/100)

            # if speedRequired + (radius/10) < realSpeed:
            #     self.throttle(speedRequired/100)

            if math.fabs(currentCarPosition['angle']) > 45:
                self.throttle(1)

            if halfCurAngle <= curAngle:
                print 'FULL RUSH'
                self.throttle(1)

            if speedRequired < realSpeed:
                self.throttle(0)
            else:
                self.throttle(speedRequired/100)

            # if curAngle > 90 and curAngle < 100 and curPart['angle'] <= 180:
            #     self.throttle(1)
            # elif curAngle > 90 and curAngle < 100 and curPart['angle'] > 180:
            #     if speedRequired < 90:
            #         self.throttle(speedRequired/100+10)
            #     else:
            #         self.throttle(1)
            # elif curAngle > 180 and curAngle < 190:
            #     self.throttle(1)

            #speedRequired = math.sqrt(radius*9.8*self.track.mu) * 10
            
            print "Turn", speedRequired, realSpeed
            return 0
            #print speedRequired, realSpeed, curAngle

        partWithTurnIdx = self.track.derictionParts.index(self.track.getNextPart(pieceIndex))
        nextTurn = None
        while partWithTurnIdx < len(self.track.derictionParts):
            if 'turn' not in self.track.derictionParts[partWithTurnIdx]:
                partWithTurnIdx += 1
            else:
                nextTurn = self.track.derictionParts[partWithTurnIdx]

                currentPieceEndLaneIdx = self.history.getCurPiecePos()['lane']['endLaneIndex']
                sign = 1
                if (nextTurn['turn'] < 0 and currentPieceEndLaneIdx == 0) or (nextTurn['turn'] > 0 and currentPieceEndLaneIdx > 0):
                    sign = -1

                radius = nextTurn['radius']+(10*sign)
                #print "RADIUS:!!!!!!!!!!!!", radius

                speedRequired = math.sqrt(radius*9.8*self.track.mu) * 10
                if speedRequired > 90:
                    partWithTurnIdx += 1
                    continue
                brakingLength = self.getBreakingLength(speedRequired)
                distanceToTurn = self.history.distanceToNextTurn(nextTurn)

                # if 'angle' in track[nextPieceIndex] and 'length' in track[pieceIndex] and realSpeed > speedRequired + ((radius/10)/2) and track[pieceIndex]['length']-20 > self.history.getCurInPieceDist():
                #     print "BREAK"
                #     self.throttle(0)

                if math.fabs(currentCarPosition['angle']) > 10 and self.brake != True:
                    self.throttle(1)

                if brakingLength < distanceToTurn and self.brake == False:
                    self.throttle(1)
                else:
                    self.brake = True

                if realSpeed <= speedRequired:
                    self.throttle(1)
                else:
                    self.throttle(0)

                print realSpeed, distanceToTurn, brakingLength, self.brake
                #print length, distanceToTurn, speedRequired, realSpeed, track[pieceIndex]
                break

        #print realSpeed

        #if nextTurn is not None:
            #print nextTurn

        #print currentPiece, nextPart

        # self.throttle(1)

        #         # if pieceIndex <= 1:
        # #     self.throttle(1)
        # # else:
        # #     self.throttle(0)

        # if 'angle' in track[pieceIndex]:
        #     radius = 110
        #     currentMu = (realSpeed * realSpeed) / (radius*9.8)
        #     print currentMu/110, pieceIndex
        # else:
        #     fnet = (realSpeed - realPrevSpeed) * 2
        #     mu = math.fabs(fnet) / (2 * 9.8)

        #     mu1 = (( self.history.getCurInPieceDist() - self.history.getPrevInPieceDist() ) / 9.8)/2

        #     print mu, mu1, pieceIndex, realSpeed, self.history.getCurInPieceDist()

        #     nextPart = self.track.getNextPart(pieceIndex)
        #     if 'turn' in nextPart:
        #         currentPieceEndLaneIdx = self.history.getCurPiecePos()['lane']['endLaneIndex']
        #         sign = 1
        #         if (nextPart['turn'] < 0 and currentPieceEndLaneIdx == 0) or (nextPart['turn'] > 0 and currentPieceEndLaneIdx > 0):
        #             sign = -1
        #         radius = nextPart['radius']+(10*sign)

        #         acceleration = (realSpeed - realPrevSpeed)
        #         speedRequired = math.sqrt(mu*9.8*radius)

            #print self.getDistance(acceleration, speedRequired, realSpeed)

    def on_turboAvailable(self, data):
        #print data
        self.turbo = data
        self.ping()

    def on_turboStart(self, data):
        print data
        self.timer = 1
        self.ping()

    def on_turboEnd(self, data):
        del self.turbo
        del self.timer
        print data
        print "Turbo ended"
        #self.throttle(0)
        self.ping()

    def on_crash(self, data):
        print("Someone crashed")
        print data
        print self.history.getCurrentPiece()
        index = self.history.getCurPieceIdx()
        print "Index", index, self.track.getPart(index), self.track.getPiece(index)

        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        #print self.biggestMu
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_lapFinished(self, data):
        print "Lap Finished. Time: ", time.time() - self.track.time
        for piece in self.track.trackPieces:
            if 'switched' in piece:
                del piece['switched']
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'lapFinished': self.on_lapFinished,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turboAvailable,
            'turboEnd': self.on_turboEnd,
            'turboStart': self.on_turboStart,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data, msg_full = msg['msgType'], msg['data'], msg
            if msg_type in msg_map:
                if msg_type == 'carPositions':
                    msg_map[msg_type](msg_full)
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Player1Bot(s, name, key)
        bot.run()
